package ch.hevs.isi.field;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * class Modbus extends socket
 */
public class ModbusAccessor extends Socket {

    private InputStream _in;
    private String ip;
    private int port;
    private OutputStream _out;
    private static ModbusAccessor theMba=null;


    ModbusAccessor(String ip, int port)throws IOException{
        super(ip,port);
        _out=this.getOutputStream();
        _in=this.getInputStream();
    }

    /**
     * method that will create only one object of class modbus
     * @return
     * @throws IOException
     */
    public static ModbusAccessor getInstance()throws IOException{
        if(theMba==null)
        {
            theMba=new ModbusAccessor("localhost",1502);
        }
        return theMba;
    }

    /**
     * method that will create the mbapheader
     * @param theBB
     * @param transID
     * @param length
     * @return
     */
    ByteBuffer setMbapHeader(ByteBuffer theBB, short transID,short length)
    {
        theBB.putShort(transID);
        theBB.putShort((short)0);
        theBB.putShort(length);
        theBB.put((byte)1);
        return theBB;
    }

    /**
     * method that will create the readFloat frame
     * @param regAddress
     * @return
     * @throws IOException
     */
    public float readFloat(int regAddress) throws IOException{
        //Modbus readFloat request
        ByteBuffer _theBB = ByteBuffer.allocate(12);
        _theBB=setMbapHeader(_theBB,(short)2,(short)(5));
        _theBB.put((byte) 0x03);
        _theBB.putShort((short)regAddress);
        _theBB.putShort((short)2);
        _out.write(_theBB.array());

        //Modbus Response
        byte[] frameToRead = new byte[13];
        _in.read(frameToRead);

        //Return value
        ByteBuffer theBB  = ByteBuffer.wrap(frameToRead);
        return theBB.getFloat(9);
    }

    /**
     * method that will create the readBoolean frame
     * @param regAddress
     * @return
     * @throws IOException
     */
    public boolean readBoolean(int regAddress)throws IOException{
        //Modbus readBoolean request
        ByteBuffer _theBB = ByteBuffer.allocate(12);
        _theBB=setMbapHeader(_theBB,(short)1,(short)(5));
        _theBB.put((byte) 0x01);
        _theBB.putShort((short)regAddress);
        _theBB.putShort((short)1);
        _out.write(_theBB.array());

        //Modbus response
        byte[] frameToRead = new byte[10];
        _in.read(frameToRead);

        //return value
        if((frameToRead[9]&0x01)==0) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * method that will create the writeFloat frame
     * @param regAddress
     * @param newValue
     * @throws IOException
     */
    public void writeFloat(int regAddress, float newValue) throws IOException {
        //Modbus writeFloat request
        ByteBuffer _theBB = ByteBuffer.allocate(17);
        _theBB=setMbapHeader(_theBB,(short)4,(short)(10));
        _theBB.put((byte)0x10);
        _theBB.putShort((short)regAddress);
        _theBB.putShort((short)2);
        _theBB.put((byte)0x04);
        _theBB.putFloat(newValue);
        _out.write(_theBB.array());

        //Modbus response
        byte[] frameToRead = new byte[12];
        _in.read(frameToRead);
    }

    /**
     * method that will create the writeBoolean frame
     * @param regAddress
     * @param newValue
     * @throws IOException
     */
    public void writeBoolean(int regAddress, boolean newValue)throws IOException {
        //Modbus writeBoolean request
        ByteBuffer _theBB = ByteBuffer.allocate(12);
        _theBB=setMbapHeader(_theBB,(short)3,(short)(5));
        _theBB.put((byte) 0x05);
        _theBB.putShort((short)regAddress);
        if(newValue==true) {
            _theBB.putShort((short) 0xFF00);
        }
        else{
            _theBB.putShort((short) 0x0000);
        }
        _out.write(_theBB.array());

        //Modbus response
        byte[] frameToRead = new byte[12];
        _in.read(frameToRead);
    }
}
