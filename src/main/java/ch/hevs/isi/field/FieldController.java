package ch.hevs.isi.field;

import ch.hevs.isi.core.*;
import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.util.Timer;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;


public class FieldController implements DatapointListener {
    private static FieldController theFc=new FieldController();

    /**
     * Private constructor for the Singleton pattern
     */
    private FieldController(){
        Timer pollTimer=new Timer();
        pollTimer.scheduleAtFixedRate(new PollTask(),0,1000);
        createAllDataRegister();
    }

    /**
     * In this methode we take the csv file and we decompose it
     * After that we create all the registers (boolean or float)
     */
    private void createAllDataRegister()
    {
        String line = "";
        String separateur = ";";
        try{
            //BufferedReader br = new BufferedReader(new FileReader("src/main/resources/ModbusEA_SIn.csv"));
            BufferedReader br = Utility.fileParser(null, "ModbusEA_SIn.csv");
            br.readLine();

            while((line = br.readLine())!= null){
                String [] tab = line.split(separateur);
                System.out.println("Label= " + tab[0] + "boolean=" + tab[1] + "isOutput"+ tab[2] +"Address" + tab[3] + "Range=" + tab[4] + "Offset=" + tab[5]);
                if(tab[1].equalsIgnoreCase("true")){
                    BooleanRegister booleanr = new BooleanRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]));
                }
                else{
                    FloatRegister floatr = new FloatRegister(tab[0], Boolean.parseBoolean(tab[2]), Integer.parseInt(tab[3]), Integer.parseInt(tab[4]), Integer.parseInt(tab[5]));
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Public methode wich call the private constructor if the is no instance of the FieldController
     * @return
     */
    public static FieldController getInstance(){
        if(theFc==null)
        {
            theFc=new FieldController();
        }
        return theFc;
    }

    /**
     * Method wich be called if the value of an object BooleanDatapoint has changed
     * @param bp
     */
    @Override
    public void onNewValue(BooleanDatapoint bp) throws IOException {
        BooleanRegister br=BooleanRegister.getRegisterFromDatapoint(bp);
        br.write();
    }

    /**
     * Method wich be called if the value of an object FloatDatapoint has changed
     * @param fp
     */
    @Override
    public void onNewValue(FloatDatapoint fp) throws IOException {
        FloatRegister fr=FloatRegister.getRegisterFromDatapoint(fp);
        fr.write();
    }

    /**
     * Unused methode in or case because we push to the field in other methodes
     * @param label
     * @param value
     */
    private void pushToField(String label,String value){
        System.out.println("Field");
        System.out.println(label+"   "+value);
        System.out.println("-------------------------------");
    }

    /**
     * A little main wich allow us to test only this class
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        FieldController.getInstance();
        FloatRegister f=new FloatRegister("FACTORY_ST",false,605,1,0);
        FloatRegister g=new FloatRegister("COAL_ST",false,601,1,0);
        BooleanRegister b= new BooleanRegister("SOLAR_CONNECT_ST",false,609);
        BooleanRegister c= new BooleanRegister("WIND_CONNECT_ST",false,613);
    }
}
