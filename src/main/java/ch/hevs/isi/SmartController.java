package ch.hevs.isi;

import ch.hevs.isi.core.BooleanDatapoint;
import ch.hevs.isi.core.Datapoint;
import ch.hevs.isi.core.FloatDatapoint;

import java.io.IOException;
import java.util.TimerTask;

public class SmartController extends TimerTask {
    float factorySetPoint = 0.5f;
    float coalSetPoint = 0f;
    float gridRefVoltage = 800;
    /**
     * Here we create our variables that are important for the "city" regulation
     */
    //Coal
    FloatDatapoint coal = (FloatDatapoint) Datapoint.getDatapointFromKey("REMOTE_COAL_SP");
    FloatDatapoint stockCoal = (FloatDatapoint) Datapoint.getDatapointFromKey("COAL_AMOUNT");
    //Factory
    FloatDatapoint factory = (FloatDatapoint) Datapoint.getDatapointFromKey("REMOTE_FACTORY_SP");
    //Battery
    FloatDatapoint batteryPercent = (FloatDatapoint) Datapoint.getDatapointFromKey("BATT_CHRG_FLOAT");
    FloatDatapoint batteryGive = (FloatDatapoint) Datapoint.getDatapointFromKey("BATT_P_FLOAT");
    //Time
    FloatDatapoint time = (FloatDatapoint) Datapoint.getDatapointFromKey("CLOCK_FLOAT");
        int step = 0;

    /**
     * In this methode we controll the city in 4 cases
     * Morning
     * Day
     * Evening
     * Night
     */
    @Override
    public void run() {
        try{
            switch (step) {
                case 0: // Time detection
                    if (time.getValue() > 0.83f || time.getValue() <= 0.20f) { // Night
                        step = 10;
                    } else if (time.getValue() > 0.20f && time.getValue() <= 0.30f) { // Start of the day
                        step = 20;
                    } else if (time.getValue() >= 0.30f && time.getValue() <= 0.70f) { // Day
                        step = 30;
                    } else if (time.getValue() > 0.65f && time.getValue() <= 0.83f) { // Start of the night
                        step = 40;
                    }
                    break;
                case 10: // Night - Start coal and stop factory
                    factory.setValue(0f);
                    if (batteryPercent.getValue() < 0.40f) {
                        coal.setValue(1f);
                    } else {
                        coal.setValue(0.6f);
                    }
                    step = 0;
                    break;
                case 20: //start of the day
                    if (stockCoal.getValue() > 0.3) {
                        coal.setValue(1f);
                    }
                    if (batteryPercent.getValue() < 0.15f) {
                        coal.setValue(coal.getValue() - 0.1f);
                    }
                    if (batteryPercent.getValue() > 0.50f && factory.getValue() < 0.50f) {
                        factory.setValue(factory.getValue() + 0.1f);
                    }
                    step = 0;
                    break;
                case 30: // Day - charge battery and factory production
                    coal.setValue(0f);
                    if (batteryPercent.getValue() > 0.55f) {
                        factory.setValue(batteryPercent.getValue() + 0.15f);
                    } else {
                        factory.setValue(0f);
                    }
                    if (batteryGive.getValue() < 0f) {
                        factory.setValue(factory.getValue() - 0.1f);
                    }
                    step = 0;
                    break;
                case 40: //start of the night
                    coal.setValue(0.6f);
                    if (batteryPercent.getValue() < 0.80f) {
                        factory.setValue(0f);
                    } else {
                        factory.setValue(factory.getValue() - 0.1f);
                    }
                    step = 0;
                    break;
            }
        }catch (IOException e)
        {

        }

    }

}
