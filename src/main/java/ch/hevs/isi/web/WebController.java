package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDatapoint;
import ch.hevs.isi.core.Datapoint;
import ch.hevs.isi.core.DatapointListener;
import ch.hevs.isi.core.FloatDatapoint;
import ch.hevs.isi.utils.Utility;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.InetSocketAddress;
import java.util.ArrayList;

public class WebController extends WebSocketServer implements DatapointListener{
    private static WebController theWbc=null;
    ArrayList<WebSocket> socketsList = new ArrayList<>();

    /**
     * Private constructor for the singleton pattern
     */
    private WebController(){
        super(new InetSocketAddress(8888));
        this.start();
    }

    /**
     * Methode called when a user opens the web site
     * @param webSocket
     * @param clientHandshake
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        socketsList.add(webSocket);
        webSocket.send("Client connected");
        System.out.println("New client" + webSocket.getLocalSocketAddress().toString());
    }

    /**
     * Methode called when a user closes the web site
     * @param webSocket
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        socketsList.remove(webSocket);
        webSocket.send("Client disconnected");
        System.out.println("Client disconnected" + webSocket.getLocalSocketAddress().toString());
    }

    /**
     * Methode called when there's a change on the web
     * Take the value
     * Set the value of the concerned datapoint
     * @param webSocket
     * @param s
     */
    @Override
    public void onMessage(WebSocket webSocket, String s){
        System.out.println(s);
        String message[]=s.split("=");
        String label=message[0];
        String value=message[1];
        try{
            if(value.equalsIgnoreCase("true")||value.equalsIgnoreCase("false"))
            {
                BooleanDatapoint bdp = (BooleanDatapoint) Datapoint.getDatapointFromKey(label);
                bdp.setValue(Boolean.valueOf(value));
            }
            else
            {
                FloatDatapoint fdp = (FloatDatapoint) Datapoint.getDatapointFromKey(label);
                fdp.setValue(Float.valueOf(value));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
    }

    @Override
    public void onStart() {

    }

    public static WebController getInstance(){
        if(theWbc==null)
        {
            theWbc=new WebController();
        }
        return theWbc;
    }

    /**
     * Method wich be called if the value of an object BooleanDatapoint has changed
     * @param bp
     */
    @Override
    public void onNewValue(BooleanDatapoint bp) {
        pushToWeb(bp.getLabel(), Boolean.toString(bp.getValue()));
    }

    /**
     * Method wich be called if the value of an object FloatDatapoint has changed
     * @param fp
     */
    @Override
    public void onNewValue(FloatDatapoint fp) {
        pushToWeb(fp.getLabel(), Float.toString(fp.getValue()));
    }
    private void pushToWeb(String label,String value){
        for (WebSocket socket : socketsList) {
            socket.send(label+"="+value);
        }

        System.out.println("Label = "+ label);
        System.out.println("Value = "+ value);
    }

    public static void main(String[] args) {
         WebController wbc = WebController.getInstance();

         BooleanDatapoint solar = new BooleanDatapoint("REMOTE_SOLAR_SW", true);
         BooleanDatapoint wind = new BooleanDatapoint("REMOTE_WIND_SW", true);
         BooleanDatapoint solar_connect = new BooleanDatapoint("SOLAR_CONNECT_ST", false);

         boolean value = true;
         while (true)
         {
             System.out.println(wind.getValue());
             /*solar.setValue(value);
             solar_connect.setValue(solar.getValue());
             value = !value;*/
             Utility.waitSomeTime(1000);
         }

    }
}
