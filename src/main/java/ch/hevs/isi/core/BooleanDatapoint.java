package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseController;
import ch.hevs.isi.field.FieldController;
import ch.hevs.isi.web.WebController;

import java.io.IOException;

/**
 * Class floatDatapoint extends of Dataoint and save an attribut value in float
 */
public class BooleanDatapoint extends Datapoint {
    /**
     * attribut that will save our value
     */
    private boolean value;

    /**
     * Constructo that save our value and call the super constructor
     * @param label
     * @param isOutput
     */
    public BooleanDatapoint(String label, boolean isOutput){
        super(label,isOutput);
    }

    /**
     * set the new value depedning of the parameters and will send it to other classes
     * @param newValue
     */
    public void setValue(boolean newValue) throws IOException {
        if(value!=newValue)
        {
            value=newValue;
            WebController.getInstance().onNewValue(this);
            try {
                DatabaseController.getInstance("","").onNewValue(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FieldController.getInstance().onNewValue(this);
        }
    }

    /**
     *
     * @return the value
     */
    public boolean getValue(){
        return value;
    }
}
