package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseController;
import ch.hevs.isi.field.FieldController;
import ch.hevs.isi.web.WebController;

import java.io.IOException;

/**
 * Class floatDatapoint extends of Dataoint and save an attribut value in float
 */
public class FloatDatapoint extends Datapoint {
    /**
     * attribut that will save the value
     */
    private float value;

    /**
     * Constructor that will save the value and call the super constructor
     * @param label
     * @param isOutput
     */
    public FloatDatapoint(String label, boolean isOutput){
        super(label,isOutput);
    }

    /**
     * Method that will change our value with a new one and will send the value to the others classes
     * @param newValue
     */
    public void setValue(float newValue) throws IOException{
        if(value!=newValue)
        {
            value=newValue;
            WebController.getInstance().onNewValue(this);
            try {
                DatabaseController.getInstance("","").onNewValue(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FieldController.getInstance().onNewValue(this);
        }
    }

    /**
     *
     * @return the value
     */
    public float getValue(){
        return value;
    }
}
