package ch.hevs.isi.core;

import ch.hevs.isi.field.ModbusAccessor;

import java.io.IOException;
import java.util.HashMap;

/**
 * Class floatRegister
 */
public class FloatRegister
{
    int address;
    int range;
    int offset;
    FloatDatapoint fdp;
    private static HashMap<FloatDatapoint, FloatRegister> hm=new HashMap<FloatDatapoint, FloatRegister>();
    /**
     * Constructor that will create a float datapoint and will put it in the register map
     * @param label
     * @param isOutput
     * @param address
     */
    public FloatRegister(String label, boolean isOutput, int address, int range, int offset )
    {
        this.address=address;
        this.range=range;
        this.offset=offset;
        fdp=new FloatDatapoint(label,isOutput);
        hm.put(fdp,this);
    }
    /**
     * method that will read the actual datapoint value
     * @throws IOException
     */
    public void read() throws IOException
    {
        fdp.setValue(ModbusAccessor.getInstance().readFloat(address)*range-offset);
        System.out.print(fdp.getLabel()+" : ");
        System.out.println(ModbusAccessor.getInstance().readFloat(address)*range-offset);
    }
    /**
     * method that will write the newValue of the datapoint
     * @throws IOException
     */
    public void write() throws IOException
    {
        ModbusAccessor.getInstance().writeFloat(address,(fdp.getValue()-offset)/range);
    }
    /**
     * method that will return a datapoint from the map depending of a key
     * @param fdp
     * @return
     */
    public static FloatRegister getRegisterFromDatapoint(FloatDatapoint fdp)
    {
        return hm.get(fdp);
    }
    /**
     * methotad that will read all boolean register datapoint value
     * @throws IOException
     */
    public static void poll()throws IOException
    {
        for (FloatRegister fr:hm.values())
        {
            fr.read();
        }
    }
}
