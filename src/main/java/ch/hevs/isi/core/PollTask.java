package ch.hevs.isi.core;
import java.io.IOException;
import java.util.TimerTask;

/**
 * Class PollTask extends timerTask
 */
public class PollTask extends TimerTask {
    @Override
    /**
     * method that will be called automatically each x seconds depends on the timer
     */
    public void run() {
        try {
            BooleanRegister.poll();
            FloatRegister.poll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
