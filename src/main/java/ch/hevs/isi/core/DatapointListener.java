package ch.hevs.isi.core;

import java.io.IOException;

/**
 * Interface wich is implemented by the controller classes
 */
public interface DatapointListener {
    public void onNewValue(BooleanDatapoint bp) throws IOException;
    public void onNewValue(FloatDatapoint fp) throws IOException;
}
