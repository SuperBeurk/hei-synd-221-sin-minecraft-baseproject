package ch.hevs.isi.core;
import ch.hevs.isi.database.DatabaseController;
import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.xml.crypto.Data;
import java.util.HashMap;
/**
 * Class Datapoint
 */
public abstract class Datapoint {
    /**
     * Map that will store all of our Datapoint objects
     */
    private static HashMap<String,Datapoint> hm=new HashMap<>();
    /**
     * Attribut that will store if it's an output or an input
     */
    private boolean isOutput;
    /**
     * Attribut that will store the name of the Datapoint
     */
    private String label;

    /**
     * Constructor will save parameters
     * @param label
     * @param isOutput
     */
    protected Datapoint(String label,boolean isOutput){
        this.isOutput=isOutput;
        this.label=label;
        hm.put(label,this);
    }

    /**
     * Method that will return a Datapoint object from the map with a parameter key to determine wich object we want
     * @param key
     * @return object of the map dependings of the key
     */
    public static Datapoint getDatapointFromKey(String key){
        return hm.get(key);
    }

    /**
     *
     * @return output or input
     */
    public boolean getIsOutput(){
        return isOutput;
    }

    /**
     *
     * @return the label of the object
     */
    public String getLabel(){
        return label;
    }
}


