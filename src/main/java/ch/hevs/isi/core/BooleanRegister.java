package ch.hevs.isi.core;

import ch.hevs.isi.field.ModbusAccessor;

import java.io.IOException;
import java.util.HashMap;

/**
 * Class booleanRegister
 */
public class BooleanRegister
{
    int address;
    BooleanDatapoint bdp;
    private static HashMap<Datapoint,BooleanRegister> hm=new HashMap<>();

    /**
     * Constructor that will create a boolean datapoint and will put it in the register map
     * @param label
     * @param isOutput
     * @param address
     */
    public BooleanRegister(String label, boolean isOutput, int address)
    {
        this.address=address;
        bdp=new BooleanDatapoint(label,isOutput);
        hm.put(bdp,this);
    }

    /**
     * method that will read the actual datapoint value
     * @throws IOException
     */
    public void read()throws IOException
    {
        bdp.setValue(ModbusAccessor.getInstance().readBoolean(address));
        System.out.print(bdp.getLabel()+" : ");
        System.out.println(ModbusAccessor.getInstance().readBoolean(address));
    }

    /**
     * method that will write the newValue of the datapoint
     * @throws IOException
     */
    public void write() throws IOException
    {
        ModbusAccessor.getInstance().writeBoolean(address,bdp.getValue());
    }

    /**
     * method that will return a datapoint from the map depending of a key
     * @param bdp
     * @return
     */
    public static BooleanRegister getRegisterFromDatapoint(BooleanDatapoint bdp)
    {
        return hm.get(bdp);
    }

    /**
     * methotad that will read all boolean register datapoint value
     * @throws IOException
     */
    public static  void poll() throws IOException
    {
        for (BooleanRegister br:hm.values())
        {
            br.read();
        }
    }
}
