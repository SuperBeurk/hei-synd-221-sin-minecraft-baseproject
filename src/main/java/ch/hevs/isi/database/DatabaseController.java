package ch.hevs.isi.database;


import ch.hevs.isi.core.BooleanDatapoint;
import ch.hevs.isi.core.DatapointListener;
import ch.hevs.isi.core.FloatDatapoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class DatabaseController implements DatapointListener {
    private static DatabaseController theDbc=null;
    String userpass;
    String link;

    /**
     * Private constructor for singleton pattern
     * @param url
     * @param groupName
     */
    private DatabaseController(String url,String groupName){
        userpass=groupName;
        link=url;
    }

    /**
     * Get the instance and call the privateconstructor if there is no instance
     * @param url
     * @param groupeName
     * @return
     */
    public static DatabaseController getInstance(String url,String groupeName){
        if(theDbc==null)
        {
            theDbc=new DatabaseController(url,groupeName);
        }
        return theDbc;
    }

    /**
     * Method wich be called if the value of an object BooleanDatapoint has changed
     * @param bp
     */
    @Override
    public void onNewValue(BooleanDatapoint bp) throws IOException {
        pushToDataBase(bp.getLabel(), Boolean.toString(bp.getValue()));
    }

    /**
     * Method wich be called if the value of an object FloatDatapoint has changed
     * @param fp
     */
    @Override
    public void onNewValue(FloatDatapoint fp) throws IOException {
        pushToDataBase(fp.getLabel(), Float.toString(fp.getValue()));
    }

    /**
     * This methode takes a label and a value and pushes it into the data base
     * The URL connection is always the same so we've created a dataBaseConnectionVariables() method (it's clearer)
     */
    public void pushToDataBase(String label,String value) throws IOException {
        HttpURLConnection myConnection = dataBaseConnectionVariables();

        OutputStreamWriter writer = new OutputStreamWriter(myConnection.getOutputStream());

        String httpMessage = label + " value=" + value;
        int len = httpMessage.getBytes().length;

        /*
         * We could have used the folowing lines instead of this one
         * writer.write(httpMessage);
         * writer.flush();
         */
        Utility.sendBytes(myConnection.getOutputStream(), httpMessage.getBytes(), 0, len);


        BufferedReader in = new BufferedReader(new InputStreamReader(myConnection.getInputStream()));
        int responseCode = myConnection.getResponseCode();

        //It's very important to place this line at this place for some abstact reasons
        while ((in.readLine()) != null) {}

        //Test to show what we'are pushing to the database
        System.out.println(responseCode);
        System.out.println("Database");
        System.out.println(label+"   "+value);
        System.out.println("-------------------------------");
        myConnection.disconnect();
    }

    /**
     * This method is just a separation of the pushToDataBase method
     * It's a bit clearer like that
     * We initialize the URL connection and return it
     * @return
     * @throws IOException
     */
    private HttpURLConnection dataBaseConnectionVariables() throws IOException {
        //Create a new URL object
        URL url = new URL(link+"write?db="+userpass);

        //create a HttpURL connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        //Identity
        String auth = userpass + ":" + Utility.md5sum(userpass);
        String encodingByte = Base64.getEncoder().encodeToString(auth.getBytes());

        connection.setRequestProperty("Authorization", "Basic " + encodingByte);

        connection.setRequestProperty("Content-Type", "binary/octet-stream");

        //Set the request method to post
        connection.setRequestMethod("POST");

        connection.setDoOutput(true);

        //Return the HttpURLConnection
        return connection;
    }

    /**
     * This main was just a way to test only this class
     * @param args
     */
    public static void main(String[] args) {
        //Database test code
        DatabaseController db = new DatabaseController("","SIn34");
        String myI = "0";
        while(true){
            for(int i = 0; i<=1000; i+=50) {
                myI = String.valueOf(i);
                try {
                    db.pushToDataBase("Val", myI);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Utility.waitSomeTime(1000);
            }
        }
    }
}
