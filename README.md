# Minecraft Base Project

## Introduction

This project has for goal to create a software that will manage a factory in the real world. In this project we will simulate this factory using MinecrafteElectricalAge, but we could use this project directly in the real world.

## Description

For this project we used the Modbus communication protocol. Modbus we allow us to read and write the values of our datapoints. Modbus will be managed by the field controller, wich will update the values of the datapoint every second. Once the values updated, they will be modified on the web and in the database.
The smart Controller component will manage our "city" in function of its different variables. The goal is to maintain the grid around 800 Volts and to take care of the battery.

### Prerequisites

For this project you need a factory. MinecraftElectricalAge could be a good option for the safe test of this project.

### Installing

To installe minecraftElectricalAge you can go on the cyberlearn of the school and follow the instruction to download it.

## Running the tests

You will need to run the .jar application to start the project

### And coding style tests

This project has been code in java language

## Authors

Developpers: Pablo STOERI, Sébastien METRAL